This is a test of integrating hscript into a haxeflixel game. Enter code into the textbox at the top (click box before typing, and it's easier to see if window was maximized) and press enter to execute that code once. The keyword 'this' is usable and refers to a FlxSprite from haxeflixel.

hscript: https://github.com/HaxeFoundation/hscript

haxeflixel: http://haxeflixel.com/