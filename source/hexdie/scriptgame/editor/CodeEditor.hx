package hexdie.scriptgame.editor;

import flixel.addons.ui.FlxUIInputText;
import flixel.FlxG;

class CodeEditor
{
	public var textbox(default, null):FlxUIInputText;

	public function new() 
	{
		textbox = new FlxUIInputText();
		textbox.width = FlxG.width;
		textbox.fieldWidth = FlxG.width;
	}
	
	public function getText()
	{
		return textbox.text;
	}
}