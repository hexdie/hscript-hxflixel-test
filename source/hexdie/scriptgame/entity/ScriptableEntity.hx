package hexdie.scriptgame.entity;

import flixel.system.FlxAssets.FlxGraphicAsset;
import hscript.Interp;
import hscript.Parser;

/**
 * ...
 * @author 
 */
class ScriptableEntity extends EntityBase
{
	private var script:String;
	private static var parser:Parser;
	private static var interpreter:Interp;
	
	public function new(?X:Float=0, ?Y:Float=0, ?SimpleGraphic:FlxGraphicAsset) 
	{
		super(X, Y, SimpleGraphic);
		
		if (parser == null)
		{
			parser = new Parser();
		}
		
		if (interpreter == null)
		{
			interpreter = new Interp();
		}
	}
	
	public function setScript(script:String)
	{
		this.script = script;
	}
	
	public function runScript():Bool
	{
		try 
		{
			var program = parser.parseString(script);
			interpreter.variables.set("this", this);
			interpreter.execute(program);
			return true;
		}
		catch (message:String)
		{
			trace(message);
			return false;
		}
	}
}