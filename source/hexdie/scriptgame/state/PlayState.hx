package hexdie.scriptgame.state;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.scaleModes.PixelPerfectScaleMode;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import hexdie.scriptgame.editor.CodeEditor;
import hexdie.scriptgame.entity.ScriptableEntity;
import openfl.Assets;

class PlayState extends FlxState
{
	var thing:ScriptableEntity;
	var editor:CodeEditor;
	
	override public function create():Void
	{
		super.create();
		
		FlxG.mouse.useSystemCursor = true;
		FlxG.scaleMode = new PixelPerfectScaleMode();
		
		editor = new CodeEditor();
		thing = new ScriptableEntity((FlxG.width / 2) - 18, (FlxG.height / 2) - 18);
		thing.loadGraphic("assets/images/player.png", true, 36, 36);
		
		add(thing);
		add(editor.textbox);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (FlxG.keys.justPressed.ENTER)
		{
			thing.setScript(editor.getText());
			var success = thing.runScript();
			
			if (!success)
			{
				//the script threw an exception, so do something about it.
			}
		}
	}
}
